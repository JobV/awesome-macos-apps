# Awesome macOS Apps

To contribute add a link and some key words to the list. 

Would appreciate someone making this a GitLab pages site.

# List

- [Hazel](https://www.noodlesoft.com) file automation, triggers, organisation
- [HazeOver](https://hazeover.com) distraction dimmer, dark mode magic
- [Tyke](https://tyke.app) simple text editor, lives in your menu bar